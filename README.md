# sylius-mailer-bundle

Mailers and e-mail template management for Symfony projects. https://packagist.org/packages/sylius/mailer-bundle

[![PHPPackages Rank](http://phppackages.org/p/sylius/mailer-bundle/badge/rank.svg)](http://phppackages.org/p/sylius/mailer-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/sylius/mailer-bundle/badge/referenced-by.svg)](http://phppackages.org/p/sylius/mailer-bundle)
